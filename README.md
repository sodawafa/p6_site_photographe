# Projet 6

## URL

### Lien projet :

[repository](https://gitlab.com/sodawafa/p6_site_photographe)

[page accueil](
https://sodawafa.gitlab.io/p6_site_photographe/
)

[page photographe](
https://sodawafa.gitlab.io/p6_site_photographe/photographer.html#243
)

### lien validation
[WCAG accueil](https://wave.webaim.org/report#/https://sodawafa.gitlab.io/p6_site_photographe/)

[WCAG photographe](https://wave.webaim.org/report#/https://sodawafa.gitlab.io/p6_site_photographe/photographer.html#243)

[W3 accueil](https://validator.w3.org/nu/?doc=https%3A%2F%2Fsodawafa.gitlab.io%2Fp6_site_photographe%2F)

[W3 photographe](https://validator.w3.org/nu/?doc=https%3A%2F%2Fsodawafa.gitlab.io%2Fp6_site_photographe%2Fphotographer.html%23243)

[AChecker](https://achecker.ca/checker/index.php)

## Technologies

> HTML5, css3, js, json, poo, eslint

## IDE

[PhpStorm](https://www.jetbrains.com/fr-fr/phpstorm/)

## Auteurs

[Wafa SODA](https://gitlab.com/sodawafa)
<[sodawafa@gmail.com](mailto:sodawafa@gmail.com)>




