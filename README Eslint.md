## Eslint

### Install

```
#npm install --g eslint
npm init
npm install eslint --save-dev
npx eslint --init
npx eslint js/script-acceuil.js
npx eslint js/script-photographer.js
```

### IDE Phpstorm:

To configure ESLint automatically in the current project, open the Settings/Preferences dialog Ctrl+Alt+S, go to
Languages and Frameworks | JavaScript | Code Quality Tools | ESLint, and select the Automatic ESLint configuration
option

### ESLint configuration for ECMAScript 6+ [optional]

> Install npm package
https://github.com/isaaceindhoven/eslint-config-es6

```
npm i -D @isaac.frontend/eslint-config-es6
#npm uninstall -D @isaac.frontend/eslint-config-es6 
#> update .eslintrc.json
  "extends": "eslint:recommended",
  "extends": ["eslint:recommended","@isaac.frontend/eslint-config-es6"],
```

## Auteurs

[Wafa SODA](https://gitlab.com/sodawafa)
<[sodawafa@gmail.com](mailto:sodawafa@gmail.com)>




