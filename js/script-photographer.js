import { MyFolder } from "./MyFolder.js";
import { Data } from "./Data.js";
import { Lightbox } from "./Lightbox.js";
import { Cookies } from "./Cookies.js";

const folder = new MyFolder();
const data = new Data();
const cookies = new Cookies();

/**
 * @param photographerId
 * @param photographers
 *
 * @returns {boolean|*}
 * @description chercher photographe
 */
function findPhotographers(photographerId, photographers) {
    if (photographers !== undefined && photographers.length > 0) {
        return photographers.filter(entry => entry.id === photographerId)[0];
    }
    return false;
}

/**
 * @description Chercher le media de chaque photographe
 * @param photographerId
 * @param media
 * @returns {boolean|*}
 */
function findMedia(photographerId, media) {
    if (media !== undefined && media.length > 0) {
        return media.filter(entry => entry.photographerId === photographerId);
    }
    return false;
}

/**
 *
 * @example id = 243
 * @returns {number} - photographer.id
 * @description retourné la valeur photographer-id enregistrée dans url ou dans cookie
 */
function getHashPhotographerId() {
    let id = parseInt(window.location.hash.substr(1));
    if (!id || isNaN(id)) {
        id = parseInt(cookies.getCookie("id"));
        if (!id || isNaN(id)) {
            window.location.href = "index.html";
        }
    }
    return id;
}

/**
 *
 *
 * @param {Object[]} photographers
 * @param {number} photographers[].id - 243
 * @param {string} photographers[].city - "London"
 * @param {string} photographers[].country - "UK"
 * @param {string} photographers[].name - "Mimi Keel"
 * @param {number} photographers[].price - 400
 * @param {string} photographers[].tagline - "Voir le beau dans le quotidien"
 * @param {string|undefined} photographers[].portrait - "MimiKeel.jpg"
 * @param {string[]} photographers[].tags - ["portrait", "events", "travel", "animals"]
 *
 * @param {Object[]} media
 * @param {string} media[].date: "2019-02-03"
 * @param {number} media[].id: 623534343
 * @param {string|undefined} media[].image: "Travel_Lonesome.jpg"
 * @param {string|undefined} media[].video
 * @param {number} media[].likes: 88
 * @param {number} media[].photographerId: 243
 * @param {number} media[].price: 45
 * @param {string[]} media[].tags: ["travel"]
 *
 * @description rechargement de la page avec les nouveaux données json.photographers et json.media
 */
function loadPage(photographers, media) {
    let id = getHashPhotographerId();
    let photographerLikes = 0;
    console.log(id);

    let photographer = {
        info: findPhotographers(id, photographers),
        media: findMedia(id, media),
    };
    console.log(photographer);

    if (photographer.info) {
        //affichage info photographer dans la bannier
        loadPhotographersInfo(photographer.info);
    } else {
        console.error("data invalide");
    }

    if (photographer.media) {


        //trie
        photographer.media.sort(byLike);
        loadPhotographersGallery(photographer.media);
        photographer.media.forEach(media => {
            photographerLikes += media.likes;
        });
        loadGlobalLikes(photographerLikes);
        loadLikes(photographerLikes);
        initCustomSelectSortBy(photographer.media);
        /*Initialisation de mon lightbox au debut de chargement de la page*/
        Lightbox.init();
        addTabIndex();
    } else {
        console.error("data invalide");
    }
}

function addTabIndex() {
    let tabIndex = 1;
    //logo
    document.querySelector("div.logo > a").tabIndex = tabIndex++;
    //boutton contacter moi
    document.querySelector("#photographer-id .button.modal-btn").tabIndex = tabIndex++;
    //tags
    document.querySelectorAll("#photographer-tags > a")
        .forEach(a => {
            a.tabIndex = tabIndex++;
        });
    //select
    document.querySelector("#sortBy .select-selected").tabIndex = tabIndex++;
    //galerie
    document.querySelectorAll(
        "#gallery-container article.photographyGalleryMainCard img.photographyGalleryMainCardImg")
        .forEach(a => {
            a.tabIndex = tabIndex++;
        });

    //formulaire
    document.querySelector(" #firstName").tabIndex = tabIndex++;
    document.querySelector(" #lastName").tabIndex = tabIndex++;
    document.querySelector("#email").tabIndex = tabIndex++;
    document.querySelector("#comment").tabIndex = tabIndex++;
    document.querySelector(".modal .close").tabIndex = tabIndex++;
    document.querySelector(".modal .btn-submit").tabIndex = tabIndex++;


    //focus logo
    document.querySelector("div.logo > a")
        .focus();

    //clavier
    document.addEventListener("keyup", function (e) {
            if ((e.key == "Enter") && (typeof e.target == "object")) {
                e.target.click();
            }
        },
    );
}

function initCustomSelectSortBy(media) {
    document.body.querySelectorAll("#sortBy .select-items div")
        .forEach(elem =>
            elem.addEventListener("click", function (e) {
                switch (e.currentTarget.innerHTML) {
                case "Popularité":
                    media.sort(byLike);
                    break;
                case "Date":
                    media.sort(byDate);
                    break;
                case "Titre":
                    media.sort(byTitle);
                    break;
                }
                loadPhotographersGallery(media);
            }),
        );
}

function reloadPhotographersGallery(media) {
    document.querySelectorAll("article.photographyGalleryMainCard")
        .forEach(n => n.remove());
    loadPhotographersGallery(media);
    loadLikes();
    Lightbox.init();
}

/**
 *
 * @param {Object[]} media
 * @param {string} media[].date: "2019-02-03"
 * @param {number} media[].id: 623534343
 * @param {string|undefined} media[].image: "Travel_Lonesome.jpg"
 * @param {number} media[].likes: 88
 * @param {number} media[].photographerId: 243
 * @param {number} media[].price: 45
 * @param {string[]} media[].tags: ["travel"]
 */
function loadPhotographersGallery(media) {
    // initiation gallery
    const galleryContainer = document.getElementById("gallery-container");
    galleryContainer.innerHTML = "";
    // affichage gallery
    media.forEach(media => {
        galleryContainer.appendChild(createMedia(media));
    });

}

function byLike(a, b) {
    if (a.likes < b.likes) {
        return 1;
    }
    if (a.likes > b.likes) {
        return -1;
    }
    return 0;
}

function byDate(a, b) {
    /**
     * @example a : media.date: "2019-02-03"
     */
    const adate = new Date(a.date).getTime();
    const bdate = new Date(b.date).getTime();
    if (adate < bdate) {
        return 1;
    }
    if (adate > bdate) {
        return -1;
    }
    return 0;
}

function byTitle(a, b) {
    if (a.image < b.image) {
        return 1;
    }
    if (a.image > b.image) {
        return -1;
    }
    return 0;
}

/**
 *
 * @param {Object} photographer
 * @param {number} photographer.id - 243
 * @param {string} photographer.city - "London"
 * @param {string} photographer.country - "UK"
 * @param {string} photographer.name - "Mimi Keel"
 * @param {number} photographer.price - 400
 * @param {string} photographer.tagline - "Voir le beau dans le quotidien"
 * @param {string|undefined} photographer.portrait - "MimiKeel.jpg"
 * @param {string[]} photographer.tags - ["portrait", "events", "travel", "animals"]
 *
 */
function loadPhotographersInfo(photographer) {

    try {
        document.querySelector("#photographer-id section")
            .setAttribute("id", "" + photographer.id);
        document.getElementById("photographer-name").innerText = photographer.name;
        document.getElementById("photographer-name-contact").innerText = photographer.name;
        document.getElementById(
            "photographer-local").innerHTML = `<span id="photographer-city">${photographer.city}</span>, <span id="photographer-country">${photographer.country}</span>`;
        document.getElementById("photographer-price").innerText = photographer.price + "€ / jour";

        let portrait = new Image();
        portrait.src = folder.getPhotographerFolderIDs(photographer.portrait);
        portrait.alt = photographer.name;
        document.getElementById("photographer-portrait").appendChild(portrait)
        document.getElementById("photographer-tagline").innerText = photographer.tagline;
        document.getElementById("photographer-tags").innerHTML = "";
        photographer.tags.forEach(
            tag => addTags(tag, document.getElementById("photographer-tags")));
    } catch (e) {
        console.log(e);
    }
}

function addTags(tag, tagsContainer) {
    let link = document.createElement("a");
    link.innerText = "#" + tag;
    link.setAttribute("data-tag", tag);
    link.addEventListener("click", event => {
        event.preventDefault();
        event.currentTarget.parentElement.querySelectorAll("a")
            .forEach(tag => tag.classList.remove("active"));
        event.currentTarget.classList.add("active");
        initFilterByTags(event.currentTarget.dataset.tag);
    });
    tagsContainer.appendChild(link);
}

function initFilterByTags(tag) {
    const photographerId = getHashPhotographerId();
    const data = new Data();
    data.ready()
        .then(() => {
            let media = data.findMediaWithTags(photographerId, tag);
            reloadPhotographersGallery(media);
        });
}

/**
 *
 * @param {Object} media
 * @param {string} media.date: "2019-02-03"
 * @param {number} media.id: 623534343
 * @param {string|undefined} media.image: "Travel_Lonesome.jpg"
 * @param {string|undefined} media.video
 * @param {number} media.likes: 88
 * @param {number} media.photographerId: 243
 * @param {number} media.price: 45
 * @param {string[]} media.tags: ["travel"]
 *
 * @returns {HTMLElement}
 */
function createMedia(media) {
    try {
        let article = document.createElement("article");
        article.classList.add("photographyGalleryMainCard");
        article.innerHTML =
            `
                <a href="#">
                    <figure class="photographyGalleryMainCardFigure">
                        <figcaption class="photographyGalleryMainCardFigcaption">
                            <div>
                                <p>${media.tags.join(", ")} ${media.date}</p>
                                <strong>${media.price} <b>€</b></strong>
                            </div>
                            <div>
                                <a href="javascript:void(0)" class="like" data-count="${media.likes}" >
                                    <strong>${media.likes}&nbsp;<span class="far fa-heart"></span></strong>
                                </a>
                            </div>
                        </figcaption>
                    </figure>
                </a>
        `;
        let mediaImg;

        /*let url = folder.getPhotographerFolderMedia(media.photographerId, media.image)*/
        if (media.video !== undefined) {
            mediaImg = createVideo(media);
        } else if (media.image !== undefined) {
            mediaImg = createImg(media);
        }
        if (mediaImg) {
            article.querySelector("figure")
                .insertBefore(mediaImg, article.querySelector("figcaption"));
        }

        return article;
    } catch (e) {
        console.log(e);
    }

}


function createImg(media) {
    let url = folder.getPhotographerFolderMedia(media.photographerId, media.image);
    let image = new Image();
    image.classList.add("photographyGalleryMainCardImg");
    image.setAttribute("alt", media.tags.join(", "));

    if (isFileExist(url)) {
        image.src = url;

    } else {
        image.src = "img/image-not-found.jpg";
        image.classList.add("imgNotfound");
    }
    return image;
}

function createVideo(media) {
    let url = folder.getPhotographerFolderMedia(media.photographerId, media.video);
    if (!isFileExist(url)) {
        return createImgNotFound();
    }
    let video = document.createElement("video"),
        source = document.createElement("source"),
        error = document.createElement("p"),
        miniature = true,
        time = "#t=0.5";
    video.classList.add("photographyGalleryMainCardImg");
    error.innerHTML = `Your browser doesn't support HTML5 video. Here is a <a href="${url}">link to the video</a> instead.`;
    source.setAttribute("type", "video/mp4");
    source.setAttribute("autoplay", "false");


    if (miniature) {
        video.setAttribute("poster", "img/iconfinder-icon (play2).svg");
        video.setAttribute("controls", "false");
        video.setAttribute("preload", "none");
        source.setAttribute("src", url);
    } else {
        video.setAttribute("controls", "controls");
        video.setAttribute("preload", "metadata");
        source.setAttribute("src", url + time);
    }
    video.appendChild(source);
    video.appendChild(error);

    return video;
}

function createImgNotFound() {
    let image = document.createElement("img");
    image.classList.add("photographyGalleryMainCardImg");
    image.setAttribute("src", "img/image-not-found.jpg");
    image.classList.add("imgNotfound");

    return image;
}

function isFileExist(url) {
    try {
        let request = new XMLHttpRequest();
        request.open("HEAD", url, false);
        request.send();
        if (request.status === 404) {
            console.log("File doesn't exist!", url);
            return false;
        }
        return true;
    } catch (e) {
        console.log(e);
    }
}

function loadLikes(photographerLikes = false) {
    const likes = document.querySelectorAll("a.like");
    if (!photographerLikes) photographerLikes = getGlobalLikes();
    likes.forEach(like => {
        like.addEventListener("click", function () {
            let count = parseInt(this.getAttribute("data-count"));
            if (false === this.classList.contains("liked")) {
                count++;
                photographerLikes++;
                console.log(photographerLikes);
                loadGlobalLikes(photographerLikes);
            } else {
                count--;
                photographerLikes--;
                console.log(photographerLikes);
                loadGlobalLikes(photographerLikes--);
            }
            this.setAttribute("data-count", count);
            const icon = this.querySelector("strong span");
            this.querySelector("strong").innerText = count + " ";
            this.querySelector("strong")
                .appendChild(icon);
            this.classList.toggle("liked");


        });
    });

}

function loadGlobalLikes(photographerLikes) {
    document.getElementById("photographer-likes").innerHTML = photographerLikes +
        "&nbsp;<span class=\"far fa-heart\"></span>";
    document.getElementById("photographer-likes")
        .setAttribute("data-globallikes", photographerLikes);
}

function getGlobalLikes() {
    let globallikes = document.getElementById("photographer-likes")
        .getAttribute("data-globallikes");
    return parseInt(globallikes);
}

data.ready()
    .then(() => {
        loadPage(data.photographers, data.media);
    });
