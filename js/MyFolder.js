const FOLDERS_PHOTOGRAPHERS_MEDIA_SRC = 'img/Sample%20Photos/';
const FOLDERS_PHOTOGRAPHERS_IDS_SRC = 'img/Sample%20Photos/Photographers ID Photos/';

export class MyFolder {

    constructor() {
        this.FOLDER_MEDIA = Object.freeze([
            {photographerId: 243, photographerFolder: 'Mimi/'},
            {photographerId: 930, photographerFolder: 'Ellie Rose/'},
            {photographerId: 82, photographerFolder: 'Tracy/'},
            {photographerId: 527, photographerFolder: 'Nabeel/'},
            {photographerId: 925, photographerFolder: 'Rhode/'},
            {photographerId: 195, photographerFolder: 'Marcel/'}
        ]);
        if (!this.FOLDER_MEDIA || !FOLDERS_PHOTOGRAPHERS_MEDIA_SRC || !FOLDERS_PHOTOGRAPHERS_IDS_SRC) {
            console.log('error in MyFolder.js!');
        }
    }

    getPhotographerFolderMedia(photographerId, image) {

        let photographer_folder_index = this.FOLDER_MEDIA.findIndex(
            folder => (folder.photographerId === photographerId)
        );
        if (photographer_folder_index === -1) return '';
        return FOLDERS_PHOTOGRAPHERS_MEDIA_SRC + this.FOLDER_MEDIA[photographer_folder_index].photographerFolder + image;
    }

    getPhotographerFolderIDs(image) {
        return FOLDERS_PHOTOGRAPHERS_IDS_SRC + image
    }
}
