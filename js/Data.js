//const URL = "https://s3-eu-west-1.amazonaws.com/course.oc-static.com/projects/Front-End+V2/P5+Javascript+%26+Accessibility/FishEyeDataFR.json"
const URL = "./FishEyeDataFR.json";

export class Data {

    constructor() {
        this.headers = new Headers();
        this.headers.append("Content-Type", "application/json;charset=UTF-8");
        this.headers.append("Access-Control-Allow-Origin", "*");
        this.isReady = this.initialize();
    }

    /**
     *
     * @returns {Promise<void>}
     */
    ready() {
        return this.isReady;
    }

    /**
     *
     * @returns {Promise<void | void>}
     */
    initialize() {
        return fetch(URL, {
            method: "GET",
            //mode: 'no-cors',
            //body: JSON.stringify(_data),
            mode: "cors",
            headers: this.headers,
        })
            .then(response => response.json())
            .then(json => {
                this.json = json;
                console.log(this.json);
                this.photographers = json.photographers;
                this.media = json.media;
            })
            .catch(error => console.log("failed in : " + error.message));
    }

    findPhotographersWithTags(query) {
        if (this.photographers && this.photographers !== undefined && this.photographers.length > 0) {
            return this.photographers.filter(entry =>
                ((entry.tags.findIndex(tag => tag === query)) !== -1),
            );
        }
    }

    findMediaWithTags(id, query) {
        if (this.media && this.media !== undefined && this.media.length > 0) {
            return this.media.filter(entry =>
                (entry.photographerId === id) &&
                ((entry.tags.findIndex(tag => tag === query)) !== -1));
        }
    }

}
