export class Lightbox {
    static init() {
        const links = Array.from(document.querySelectorAll(
            "img.photographyGalleryMainCardImg[src$=\".png\"],img.photographyGalleryMainCardImg[src$=\".jpg\"], img.photographyGalleryMainCardImg[src$=\".jpeg\"]"));
        const gallery = links.map(link => link.getAttribute("src"));
        links.forEach(link => link.addEventListener("click", e => {
            e.preventDefault();
            let linkImageSelected = e.currentTarget.getAttribute(("src"), gallery);
            new Lightbox(linkImageSelected, gallery);
        }));
    }

    constructor(url, images) {
        this.images = images;
        this.oldImage = url;
        const element = this.buildDOM(url);
        document.body.appendChild(element);
        //html créer
        this.onload();
    }

    onload() {
        document.querySelector(".lightbox_close")
            .addEventListener("click", this.close.bind(this));
        document.querySelector(".lightbox_next")
            .addEventListener("click", this.next.bind(this));
        document.querySelector(".lightbox_prev")
            .addEventListener("click", this.prev.bind(this));

        this.onKeyUpFunc = this.onKeyUp.bind(this);
        document.addEventListener("keyup", this.onKeyUpFunc);


    }


    /* Ferme la lightbox par clavier*/
    onKeyUp(e) {
        switch (e.key) {
        case "ArrowLeft":
            this.prev(e);
            break;
        case "ArrowRight":
            this.next(e);
            break;
        case "Escape":
            this.close(e);
            break;
        }
    }


    /* Ferme la lightbox*/
    close(e) {
        e.preventDefault();
        const lightboxContainer = document.querySelector("div.lightbox");
        lightboxContainer.classList.add("fadeOut");
        document.removeEventListener("keyup", this.onKeyUpFunc);
        window.setTimeout(() => {
            lightboxContainer.remove();
        }, 600);
    }

    next(e) {
        e.preventDefault();
        let i = this.images.findIndex(image => (image === this.oldImage));
        if (i === -1 || i === this.images.length - 1) {
            i = -1;
        }
        this.loadImage(this.images[i + 1]);
    }

    prev(e) {
        e.preventDefault();
        let i = this.images.findIndex(image => image === this.oldImage);
        if (i === -1 || i === 0) {
            i = this.images.length;
        }
        this.loadImage(this.images[i - 1]);
    }

    /**
     * @param {string} url URL de l'image
     */
    loadImage(url) {
        /*this.oldImage = null*/
        const container = document.querySelector(".lightbox_container");
        container.innerHTML = "";/*effacer tout le contenu html*/

        const loader = document.createElement("div");
        loader.classList.add("lightbox_loader");
        container.appendChild(loader);

        const image = new Image();
        image.onload = () => {
            container.removeChild(loader);
            container.appendChild(image);
        };
        this.oldImage = url;
        image.src = url;
    }

    /*pour créer elements HTML*/
    buildDOM(url) {
        const dom = document.createElement("div");
        dom.classList.add("lightbox");
        dom.innerHTML = `
            <button class="lightbox_close"></button>
            <button class="lightbox_next"></button>
            <button class="lightbox_prev"></button>
            <div class="lightbox_container">
                <img src="${url}" alt="Animals_Rainbow">
            </div>
            `;
        return dom;
    }

}

/*Initialisation de lightbox a l'aide d'une methode static
* Dans la fonction init on va devoir selectioné tous les liens qui méne vers jpg.
* Aprés je vais séléctoner tous les liens qui on a un attrebut href qui finis par un point png.
* Maintenant quand a fait ce "querySelectorAll" on va pouvoir parcouri l'ensemble de nos liens a l'aide de proprieter "forEach"
*   Expliqation : pour chaque lien on va rajouter un listener et on dira lorsque tu click
* je veux qui tu lance une fonctionqui prenand un parametre d'evenement.
* .forEach(link => link.addEventListener('click', e => {
                e.preventDefault() : pour stoper le comportement par defaut
                new Lightbox() : pour intialiser une nouvelle lightbox je besoin de url qui je dois charger
                * pour cela je fais un :
                * e : pour recuperer mon lien
                * currentTarget : permetera de selectioner le lien sur lequelle je viens l'appliquer
                *getAttribute : pur recépurer l'attrebu href
            }))
            *   <img src="${url}" width="800px" height="800px" alt="Animals_Rainbow">
            * */
