const modal = document.querySelector(".modal"),
    modalBtn = document.querySelectorAll(".modal-btn"),
    closeBtn = document.querySelector(".close"),
    btnSubmit = document.querySelector(".btn-submit"),
    firstName = document.getElementById("firstName"),
    lastName = document.getElementById("lastName"),
    email = document.getElementById("email");

/*Excite l'événement du  formulaire*/
modalBtn.forEach((btn) => btn.addEventListener("click", launchModal));
closeBtn.addEventListener("click", function () {
    closeModal();
});
btnSubmit.addEventListener("click", validate);

// launch modal form
function launchModal() {
    window.scrollTo(0, 0);
    modal.style.display = "block";
    initializeValidation();
}


function closeModal() {
    modal.style.display = "none";
}

function resetModal() {
    closeModal();
    document.getElementById("myForm")
        .reset();
    document.getElementById("myForm").style.display = "block";
    document.getElementById("loader").style.display = "none";
    document.getElementById("confirmation").style.display = "none";
}


/*Enregistrement des erreurs*/
function sendError(elem, message = "Merci de remplire ce champ") {
    let error = elem.parentElement.querySelector(".error");
    if (error !== undefined) {
        if (message.length > 0) {
            error.innerHTML = message;
            error.style.display = "block";
        } else {
            error.innerHTML = message;
            error.style.display = "none";
        }
    } else {
        alert(message);
    }
}

function checkError(input, message) {
    if (input.validity.valid) {
        input.classList.remove("invalid");
        sendError(input, "");
        return false;
    } else {
        input.classList.add("invalid");
        sendError(input, message);
        return true;
    }
}


function initializeValidation() {
    firstName.addEventListener("input", function () {
        checkError(firstName, "Veuillez entrer deux caractères ou plus");
    }, false);

    lastName.addEventListener("input", function () {
        checkError(lastName, "Veuillez entrer deux caractères ou plus");
    }, false);

    email.addEventListener("input", function () {
        checkError(email, "Veuillez entrer un mail valide");
    }, false);
}

function validate(e) {
    e.preventDefault();
    let testForm = true;
    if (
        checkError(firstName, "Veuillez entrer deux caractères ou plus.") ||
        checkError(lastName, "Veuillez entrer deux caractères ou plus.") ||
        checkError(email, "Veuillez entrer un mail valide.")

    ) {
        testForm = false;
    }

    /*envoie des données et message de confirmation*/
    if (testForm) {
        sendData();
    }

    /*bloqué le rechargement de la page*/
    return false;
}


function sendData() {
    document.getElementById("myForm").style.display = "none";
    document.getElementById("loader").style.display = "block";
    /*on va executer la fonction (showConfirmation) message de confirmation aprés 2secondes*/
    setTimeout(showConfirmation, 2000);
}

function showConfirmation() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("confirmation").style.display = "block";
    /*on va fermé le modal aprés 3 secondes*/
    setTimeout(resetModal, 3000);
}
