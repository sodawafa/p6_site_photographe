import { MyFolder } from "./MyFolder.js";
import { Data } from "./Data.js";
import { Cookies } from "./Cookies.js";

const folder = new MyFolder();
const data = new Data();
const cookies = new Cookies();

function addTabIndex() {
    let tabIndex = 1;
    document.querySelector("div.logo > a").tabIndex = tabIndex++;
    /*document.querySelector("nav#global-tags").tabIndex = tabIndex++;*/
    document.querySelectorAll("nav#global-tags > a")
        .forEach(a => {
            a.tabIndex = tabIndex++;
        });
    /*document.querySelector("#gallery-container").tabIndex = tabIndex++;*/
    document.querySelectorAll("article.galleryCard")
        .forEach(article => {
            article.tabIndex = tabIndex++;
        });

    document.querySelector("div.logo > a")
        .focus();

    document.addEventListener("keyup", function (e) {
            if ((e.key == "Enter") && (typeof e.target == "object")) {
                e.target.click();
            }
        },
    );

}

window.onscroll = function () {
    const posY = 500;
    if (window.scrollY > posY) {
        document.querySelector("#gallery-link").style.display = "block";
    } else {
        document.querySelector("#gallery-link").style.display = "none";
    }
};


data.ready()
    .then(() => {
        loadPage(data.photographers);
    });


/**
 *
 * @param {Object[]} photographers
 * @param {number} photographers[].id - 243
 * @param {string} photographers[].city - "London"
 * @param {string} photographers[].country - "UK"
 * @param {string} photographers[].name - "Mimi Keel"
 * @param {number} photographers[].price - 400
 * @param {string} photographers[].tagline - "Voir le beau dans le quotidien"
 * @param {string|undefined} photographers[].portrait - "MimiKeel.jpg"
 * @param {string[]} photographers[].tags - ["portrait", "events", "travel", "animals"]
 *
 * @description rechargement de la page avec les nouveaux données json.photographers
 */
function loadPage(photographers) {
    let globalTags = [];

    if (photographers) {
        loadGallery(photographers);
        photographers.map(photographer => globalTags.push(...photographer["tags"]));
        globalTags = globalTags.filter((tag, index) => {
            return globalTags.indexOf(tag) === index;
        });
        console.log(globalTags);
        globalTags.forEach(tag => addTags(tag, document.getElementById("global-tags")));
        addTabIndex();
    } else {
        console.error("data invalide");
    }
}

function loadGallery(photographers) {
    const galleryContainer = document.getElementById("gallery-container");
    galleryContainer.innerHTML = "";
    //affichage gallery
    photographers.forEach(photographer => {
        galleryContainer.appendChild(createMedia(photographer));
    });
}


function initFilterByTags(tag) {
    const data = new Data();
    data.ready()
        .then(() => {
            let photographers = data.findPhotographersWithTags(tag);
            loadGallery(photographers);
        });
}

/**
 *
 * @param {Object} photographer
 * @param {number} photographer.id - 243
 * @param {string} photographer.city - "London"
 * @param {string} photographer.country - "UK"
 * @param {string} photographer.name - "Mimi Keel"
 * @param {number} photographer.price - 400
 * @param {string} photographer.tagline - "Voir le beau dans le quotidien"
 * @param {string|undefined} photographer.portrait - "MimiKeel.jpg"
 * @param {string[]} photographer.tags - ["portrait", "events", "travel", "animals"]
 *
 * @returns {HTMLElement}
 */
function createMedia(photographer) {
    let article = document.createElement("article");
    article.classList.add("galleryCard");
    let articleContainer = document.createElement("a");
    articleContainer.href = "photographer.html";
    let tagsContainer = document.createElement("div");

    articleContainer.innerHTML =
        ` <figure class="galleryCardFigure" id="${photographer.id}">
                        <figcaption class="galleryCardFigureCaption">
                            <p class="galleryCardFigureCaptionTitle">
                                ${photographer.name}
                            </p>
                        </figcaption>
            </figure>
            <div class="galleryCardDesc">
                <h2>${photographer.city}, ${photographer.country}</h2>
                <h3>${photographer.tagline}</h3>
                <span>${photographer.price}€/jour</span>
            </div>`;
    photographer.tags.forEach(tag => addTags(tag, tagsContainer));
    articleContainer.querySelector("figure")
        .insertBefore(createImg(photographer), article.querySelector("figcaption"));
    articleContainer.addEventListener("click", event => {
        event.preventDefault();
        window.location.href = setHashPhotographerId(event.currentTarget.href, photographer.id);

    });

    article.appendChild(articleContainer);
    article.appendChild(tagsContainer);
    return article;
}

function setHashPhotographerId(url, id) {
    cookies.setCookie("id", id);
    return url + "#" + id;
}

function addTags(tag, tagsContainer) {
    let link = document.createElement("a");
    link.innerText = "#" + tag;
    link.setAttribute("data-tag", tag);
    link.addEventListener("click", event => {
        event.preventDefault();
        event.currentTarget.parentElement.querySelectorAll("a")
            .forEach(tag => tag.classList.remove("active"));
        event.currentTarget.classList.add("active");
        initFilterByTags(event.currentTarget.dataset.tag);
    });
    tagsContainer.appendChild(link);
}

function createImg(photographer) {
    let url = folder.getPhotographerFolderIDs(photographer.portrait);
    let image = new Image();
    image.classList.add("galleryCardFigureImg");
    image.setAttribute("alt", photographer.name);
    if (isFileExist(url) && (photographer.portrait !== undefined)) {
        image.src = url;
    } else {
        image.src = "img/image-not-found.jpg";
        image.classList.add("imgNotfound");
    }
    return image;
}

function isFileExist(url) {
    try {
        let request = new XMLHttpRequest();
        request.open("HEAD", url, false);
        request.send();
        if (request.status === 404) {
            console.log("File doesn't exist!", url);
            return false;
        }
        return true;
    } catch (e) {
        console.log(e);
    }
}
